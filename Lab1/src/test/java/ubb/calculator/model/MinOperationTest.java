package ubb.calculator.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class MinOperationTest {
    @Test
    public void testSimpleMinFunction() {
        List<Double> operands = Arrays.asList(1.0, 2.0);
        Operation operation = new MinOperation(operands);
        assertThat(operation.execute(), equalTo(1.0));
    }

    @Test
    public void testChainedMinFunction() {
        List<Double> operands = Arrays.asList(4.0, 0.0, 3.0, 25.0, 6.0);
        Operation operation = new MinOperation(operands);
        assertThat(operation.execute(), equalTo(0.0));
    }
}