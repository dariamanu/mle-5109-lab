package ubb.calculator.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class MultiplyOperationTest{
    @Test
    public void testSimpleMultiplication() {
        List<Double> operands = Arrays.asList(2.0, 3.0);
        Operation operation = new MultiplyOperation(operands);
        assertThat(operation.execute(), equalTo(6.0));
    }

    @Test
    public void testChainedMultiplications() {
        List<Double> operands = Arrays.asList(1.0, 2.0, 3.0, 4.0);
        Operation operation = new MultiplyOperation(operands);
        assertThat(operation.execute(), equalTo(24.0));
    }
}