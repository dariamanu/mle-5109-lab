package ubb.calculator.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class SubtractOperationTest{
    @Test
    public void testSimpleSubtraction() {
        List<Double> operands = Arrays.asList(2.0, 1.0);
        Operation operation = new SubtractOperation(operands);
        assertThat(operation.execute(), equalTo(1.0));
    }

    @Test
    public void testChainedSubtractions() {
        List<Double> operands = Arrays.asList(7.2, 5.2, 3.0);
        Operation operation = new SubtractOperation(operands);
        assertThat(operation.execute(), equalTo(-1.0));
    }
}