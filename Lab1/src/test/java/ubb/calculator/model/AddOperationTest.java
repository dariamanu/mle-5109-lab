package ubb.calculator.model;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class AddOperationTest{

    @Test
    public void testSimpleAddition() {
        List<Double> operands = Arrays.asList(1.0, 2.0);
        Operation operation = new AddOperation(operands);
        assertThat(operation.execute(), equalTo(3.0));
    }

    @Test
    public void testChainedAdditions() {
        List<Double> operands = Arrays.asList(1.0, 2.0, 3.1, 4.2);
        Operation operation = new AddOperation(operands);
        assertThat(operation.execute(), equalTo(10.3));
    }
}