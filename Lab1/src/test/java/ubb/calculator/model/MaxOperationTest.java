package ubb.calculator.model;

import org.junit.Test;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.List;

public class MaxOperationTest {
    @Test
    public void testSimpleMaxFunction() {
        List<Double> operands = Arrays.asList(1.0,2.0);
        Operation operation = new MaxOperation(operands);
        assertThat(operation.execute(), equalTo(2.0));
    }

    @Test
    public void testChainedMaxFunction() {
        List<Double> operands = Arrays.asList(4.0, 0.0, 3.0, 25.0, 6.0);
        Operation operation = new MaxOperation(operands);
        assertThat(operation.execute(), equalTo(25.0));
    }
}