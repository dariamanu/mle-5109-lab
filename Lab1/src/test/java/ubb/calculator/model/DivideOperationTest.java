package ubb.calculator.model;


import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class DivideOperationTest{
    @Test
    public void testSimpleDivision() {
        List<Double> operands = Arrays.asList(10.0, 2.0);
        Operation operation = new DivideOperation(operands);
        assertThat(operation.execute(), equalTo(5.0));
    }

    @Test
    public void testChainedDivisions() {
        List<Double> operands = Arrays.asList(48.0, 12.0, 2.0, 2.0);
        Operation operation = new DivideOperation(operands);
        assertThat(operation.execute(), equalTo(1.0));
    }
}