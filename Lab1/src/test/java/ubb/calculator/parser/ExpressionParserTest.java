package ubb.calculator.parser;


import org.junit.Before;
import org.junit.Test;
import ubb.calculator.factory.OperationFactory;

import ubb.calculator.model.Operation;

import static org.hamcrest.Matchers.equalTo;

import static org.hamcrest.Matchers.in;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertThrows;

public class ExpressionParserTest {
    public OperationFactory operationFactory;
    public ExpressionParser parser;

    @Before
    public void setUp() {
        operationFactory = new OperationFactory();
        parser = new ExpressionParser(operationFactory);
    }

    @Test
    public void testGetAddOperationWhenTotalIsNotPartOfOperands() throws IllegalOperationException {
        String inputLine = "2.0 + 3.0";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(5.0));
    }

    @Test
    public void testGetAddOperationWhenTotalIsPartOfOperands() throws IllegalOperationException {
        String inputLine = "+ 3.0";
        Double total = 2.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(5.0));
    }

    @Test
    public void testGetSubtractOperationWhenTotalIsNotPartOfOperands() throws IllegalOperationException {
        String inputLine = "5.0 - 2.0";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(3.0));
    }

    @Test
    public void testGetSubtractOperationWhenTotalIsPartOfOperands() throws IllegalOperationException {
        String inputLine = "- 2.0";
        Double total = 5.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(3.0));
    }

    @Test
    public void testGetMultiplyOperationWhenTotalIsNotPartOfOperands() throws IllegalOperationException {
        String inputLine = "5.0 * 2.0";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(10.0));
    }

    @Test
    public void testGetMultiplyOperationWhenTotalIsPartOfOperands() throws IllegalOperationException {
        String inputLine = "* 2.0";
        Double total = 5.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(10.0));
    }

    @Test
    public void testGetDivideOperationWhenTotalIsNotPartOfOperands() throws IllegalOperationException {
        String inputLine = "10.0 / 2.0";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(5.0));
    }

    @Test
    public void testGetDivideOperationWhenTotalIsPartOfOperands() throws IllegalOperationException {
        String inputLine = "/ 2.0";
        Double total = 10.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(5.0));
    }

    @Test
    public void testGetMaxFunction() throws IllegalOperationException {
        String inputLine = "max(1, 2, 0.2, 7, 4)";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(7.0));
    }

    @Test
    public void testGetMinFunction() throws IllegalOperationException {
        String inputLine = "min(1, 2, 0.2, 7, 4)";
        Double total = 0.0;
        Operation operation = parser.getOperation(total, inputLine);
        total = operation.execute();
        assertThat(total, equalTo(0.2));
    }

    @Test
    public void testThrowExceptionForIllegalOperationType() {
        String inputLine = "10 % 3";
        Double total = 0.0;
        IllegalOperationException exception = assertThrows(IllegalOperationException.class,
                () -> parser.getOperation(total, inputLine));
        assertThat(exception.getMessage(), equalTo("Operation is undefined"));
    }

    @Test
    public void testChainingOfOperations() throws IllegalOperationException {
        Double total = 0.0;
        Operation operation = parser.getOperation(total, "1+3");
        total = operation.execute();
        assertThat(total, equalTo(4.0));
        operation = parser.getOperation(total, "2+3");
        total = operation.execute();
        assertThat(total, equalTo(5.0));
        operation = parser.getOperation(total, "*5");
        total = operation.execute();
        assertThat(total, equalTo(25.0));
        operation = parser.getOperation(total, "/5");
        total = operation.execute();
        assertThat(total, equalTo(5.0));
        operation = parser.getOperation(total, "+3");
        total = operation.execute();
        assertThat(total, equalTo(8.0));
        operation = parser.getOperation(total, "max(1,2,3)");
        total = operation.execute();
        assertThat(total, equalTo(3.0));
        operation = parser.getOperation(total, "-2");
        total = operation.execute();
        assertThat(total, equalTo(1.0));
        operation = parser.getOperation(total, "min(1,2,3)");
        total = operation.execute();
        assertThat(total, equalTo(1.0));
    }
}