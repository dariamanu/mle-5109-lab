package ubb.calculator;

import ubb.calculator.model.Operation;
import ubb.calculator.parser.ExpressionParser;
import ubb.calculator.parser.IllegalOperationException;

import java.util.Scanner;

public class Calculator {
    private final ExpressionParser parser;
    private final Scanner input;
    private double total;

    public Calculator(ExpressionParser parser) {
        this.parser = parser;
        this.input = new Scanner(System.in);
        this.total = 0;
    }

    public void run() {
        displayOptions();
        String crtLine;
        while(!(crtLine = input.nextLine()).equals("exit")) {
            try {
                Operation operation = parser.getOperation(total, crtLine);
                total = operation.execute();
                System.out.println("TOTAL: " + total);
            } catch (IllegalOperationException exception) {
                System.out.println(exception.getMessage());
            }
        }
    }

    private void displayOptions() {
        System.out.println("WELCOME!");
        System.out.println("OPERATIONS: +, -, *, /, max, min");
        System.out.println("TYPE exit TO STOP");
        System.out.println("TOTAL: " + total);
    }
}
