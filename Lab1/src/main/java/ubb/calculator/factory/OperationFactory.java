package ubb.calculator.factory;

import ubb.calculator.model.*;

import java.util.List;

public class OperationFactory {
    public Operation createOperation(OperationType type, List<Double> operands) {
        switch (type) {
            case ADD:
                return new AddOperation(operands);
            case SUBTRACT:
                return new SubtractOperation(operands);
            case MULTIPLY:
                return new MultiplyOperation(operands);
            case DIVIDE:
                return new DivideOperation(operands);
            case MAX:
                return new MaxOperation(operands);
            case MIN:
                return new MinOperation(operands);
        }
        return null;
    }
}
