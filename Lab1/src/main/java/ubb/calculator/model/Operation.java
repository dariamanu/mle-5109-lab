package ubb.calculator.model;

import java.util.List;

public abstract class Operation {
    private final List<Double> operands;

    public Operation(List<Double> operands) {
        this.operands = operands;
    }

    public Double execute() {
        Double result = operands.get(0);
        for (int i = 1; i < operands.size(); i++) {
            result = executeOneOperation(result, operands.get(i));
        }
        return result;
    }

    protected abstract Double executeOneOperation(Double val1, Double val2);
}
