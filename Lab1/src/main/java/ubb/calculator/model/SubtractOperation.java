package ubb.calculator.model;

import java.util.List;

public class SubtractOperation extends Operation {
    public SubtractOperation(List<Double> operands) {
        super(operands);
    }

    @Override
    protected Double executeOneOperation(Double val1, Double val2) {
        return val1 - val2;
    }
}
