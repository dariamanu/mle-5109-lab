package ubb.calculator.model;

import java.util.List;

public class MinOperation extends Operation {
    public MinOperation(List<Double> operands) {
        super(operands);
    }

    @Override
    protected Double executeOneOperation(Double val1, Double val2) {
        return (val1 < val2) ? val1 : val2;
    }
}
