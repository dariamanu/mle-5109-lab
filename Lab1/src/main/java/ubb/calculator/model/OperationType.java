package ubb.calculator.model;

public enum OperationType{
    ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    MAX,
    MIN
}
