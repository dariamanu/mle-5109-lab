package ubb.calculator.model;

import java.util.List;

public class MultiplyOperation extends Operation {
    public MultiplyOperation(List<Double> operands) {
        super(operands);
    }

    @Override
    protected Double executeOneOperation(Double val1, Double val2) {
        return val1 * val2;
    }
}
