package ubb.calculator.parser;

public class IllegalOperationException extends Exception {
    public IllegalOperationException(String message) {
        super(message);
    }
}
