package ubb.calculator.parser;

import ubb.calculator.factory.OperationFactory;
import ubb.calculator.model.Operation;
import ubb.calculator.model.OperationType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ExpressionParser {
    private static final String BASE_OPERATION_PATTERN = "(\\%s?[1-9]*\\d(\\.\\d+)*)+";
    private static final String MAX_OPERATION_PATTERN = "max\\([1-9]*\\d(\\.\\d+)*(,[1-9]*\\d(\\.\\d+)*)*\\)";
    private static final String MIN_OPERATION_PATTERN = "min\\([1-9]*\\d(\\.\\d+)*(,[1-9]*\\d(\\.\\d+)*)*\\)";
    private static final String MAX_FUNCTION = "max";
    private static final String MIN_FUNCTION = "min";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSED_BRACKET = ")";
    private static final String COMMA = ",";
    private static final String ADD_OPERATOR = "+";
    private static final String SUBTRACT_OPERATOR = "-";
    private static final String MULTIPLY_OPERATOR = "*";
    private static final String DIVIDE_OPERATOR = "/";
    private static final String SPACE = " ";
    private static final String EMPTY_STRING = "";

    private final OperationFactory operationFactory;

    public ExpressionParser(OperationFactory operationFactory) {
        this.operationFactory = operationFactory;
    }

    public Operation getOperation(double total, String line) throws IllegalOperationException {
        line = line.replaceAll(SPACE, EMPTY_STRING);
        List<Double> operands = new ArrayList<>();

        if (checkArithmeticOperationType(line, ADD_OPERATOR)) {
            prepareArithmeticOperands(total, operands, line, "\\" + ADD_OPERATOR);
            return operationFactory.createOperation(OperationType.ADD, operands);
        }

        if (checkArithmeticOperationType(line, SUBTRACT_OPERATOR)) {
            prepareArithmeticOperands(total, operands, line, SUBTRACT_OPERATOR);
            return operationFactory.createOperation(OperationType.SUBTRACT, operands);
        }

        if (checkArithmeticOperationType(line, MULTIPLY_OPERATOR)) {
            prepareArithmeticOperands(total, operands, line, "\\" + MULTIPLY_OPERATOR);
            return operationFactory.createOperation(OperationType.MULTIPLY, operands);
        }

        if (checkArithmeticOperationType(line, DIVIDE_OPERATOR)) {
            prepareArithmeticOperands(total, operands, line, DIVIDE_OPERATOR);
            return operationFactory.createOperation(OperationType.DIVIDE, operands);
        }

        if (checkMaxOperationType(line)) {
            prepareMaxMinFunctionOperands(line, operands);
            return operationFactory.createOperation(OperationType.MAX, operands);
        }

        if (checkMinOperationType(line)) {
            prepareMaxMinFunctionOperands(line, operands);
            return operationFactory.createOperation(OperationType.MIN, operands);
        }

        throw new IllegalOperationException("Operation is undefined");
    }

    private boolean checkArithmeticOperationType(String line, String operator) {
        String pattern = String.format(BASE_OPERATION_PATTERN, operator);
        return Pattern.matches(pattern, line);
    }

    private boolean checkMaxOperationType(String line) {
        return Pattern.matches(MAX_OPERATION_PATTERN, line);
    }

    private boolean checkMinOperationType(String line) {
        return Pattern.matches(MIN_OPERATION_PATTERN, line);
    }

    private boolean totalIsPartOfOperands(String line, String operator) {
        return Character.toString(line.charAt(0)).equals(operator);
    }

    private void prepareArithmeticOperands(Double total, List<Double> operands, String line, String operator) {
        String[] operandStrings = line.split(operator);

        if (operator.contains("\\")) {
            if (operator.contains(ADD_OPERATOR)) {
                operator = ADD_OPERATOR;
            } else {
                operator = MULTIPLY_OPERATOR;
            }
        }

        if (totalIsPartOfOperands(line, operator)) {
            operands.add(total);
        }
        operands.addAll(Arrays.stream(operandStrings)
                .filter(op -> !op.equals(EMPTY_STRING))
                .map(Double::parseDouble)
                .collect(Collectors.toList()));
    }

    private void prepareMaxMinFunctionOperands(String line, List<Double> operands) {
        if (line.contains(MAX_FUNCTION)) {
            line = line.replace(MAX_FUNCTION, EMPTY_STRING);
        } else {
            line = line.replace(MIN_FUNCTION, EMPTY_STRING);
        }
        line = line.replace(OPEN_BRACKET, EMPTY_STRING);
        line = line.replace(CLOSED_BRACKET, EMPTY_STRING);
        String[] operandStrings = line.split(COMMA);
        operands.addAll(Arrays.stream(operandStrings)
                .filter(op -> !op.equals(EMPTY_STRING))
                .map(Double::parseDouble)
                .collect(Collectors.toList()));
    }
}
