package ubb.calculator;

import ubb.calculator.factory.OperationFactory;
import ubb.calculator.parser.ExpressionParser;

public class Main {
    public static void main(String[] args) {
        OperationFactory factory = new OperationFactory();
        ExpressionParser parser = new ExpressionParser(factory);
        Calculator calculator = new Calculator(parser);
        calculator.run();
    }
}
